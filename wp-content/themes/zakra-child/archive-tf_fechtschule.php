<?php
/**
 * The template for displaying archive pages
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zakra
 */

get_header();
?>

    <div id="primary" class="content-area primary-full-width">
        <?php echo apply_filters( 'zakra_after_primary_start_filter', false ); // WPCS: XSS OK. ?>

        <?php if ( have_posts() ) : ?>

            <header class="page-header">
                <?php
                zakra_entry_title();
                ?>
            </header><!-- .page-header -->

            <?php
            do_action( 'zakra_before_posts_the_loop' );
            /* Start the Loop */

            ?><div class="tf_fechtschule-overview-container"><?php
            while ( have_posts() ) :
                the_post();

            ?>

            <?php
                // do not display "static" pages
                $acf_data = get_fields();
                if ($acf_data['art_des_beitrags'] != 'static' && $acf_data['art_des_beitrags'] != 'translation') {
                    /*
                     * Include the Post-Type-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                     */
                    $translation = $acf_data['translation'];
                    $place = [
                        'de' => get_field('name', $acf_data['ort']->ID),
                        'en' => get_field('name_en', $acf_data['ort']->ID)
                    ];
                    $link = [
                        'de' => get_the_permalink(),
                        'en' => $translation ? get_the_permalink($acf_data['translation']->ID) : ''
                    ];
                    $card_image = [];
                    if ($acf_data['symbolbild']) {
                        $card_image = [
                            'id' => $acf_data['symbolbild']['ID'],
                            'attr' => [
                                'src'    => $acf_data['symbolbild']['sizes']['tf_fechtschule-highlight-overview-large'],
                                'srcset' => $acf_data['symbolbild']['sizes']['tf_fechtschule-highlight-overview-small'] . ', ' . $acf_data['symbolbild']['sizes']['tf_fechtschule-highlight-overview-large'] . ' 2x'
                            ]
                        ];
                    }
                    elseif ($acf_data['highlightimage']) {
                        $card_image = [
                            'id' => $acf_data['highlightimage']['ID'],
                            'attr' => [
                                'src'    => $acf_data['highlightimage']['sizes']['tf_fechtschule-highlight-overview-large'],
                                'srcset' => $acf_data['highlightimage']['sizes']['tf_fechtschule-highlight-overview-small'] . ', ' . $acf_data['highlightimage']['sizes']['tf_fechtschule-highlight-overview-large'] . ' 2x'
                            ]
                        ];
                    }
                    ?>
                    <div class="tf_fechtschule-card <?php echo !empty($acf_data['fechtschule_archivieren']) ? '' : 'tf_fechtschule-card-not-archived' ?>">
                        <?php echo !empty($card_image) ? wp_get_attachment_image($card_image['id'], [500, 250], false, $card_image['attr']) : ''; ?>
                        <div class="tf_fechtschule-card-text">
                            <h3><?php the_title() ?></h3>
                            <?php echo !empty($translation) ? '<h4>' . get_the_title($translation->ID) . '</h4>' : ''; ?>
                            <p><?php echo date_formatter($acf_data['beginn'], 'd.M.', 'de_DE') ?> &ndash; <?php echo date_formatter($acf_data['ende'], 'd.M.y', 'de_DE') ?> <br> <?php echo $place['de']; echo !empty($place['en']) ? ' / <i>' . $place['en'] . '</i>' : '' ?></p>
                            <p class="tf_fechtschule-card-links"><a href="<?php echo $link['de'] ?>" style="display:inline-block; margin-right:10px;">Mehr erfahren</a> <?php echo $link['en'] ? ' | <a href="' . $link['en'] . '" style="display:inline-block; margin-left:10px;">Read more</a>' : '' ?></p>
                        </div>
                    </div>
                    <?php

                }

            endwhile;
            ?></div><?php
            do_action( 'zakra_after_posts_the_loop' );

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>

        <?php echo apply_filters( 'zakra_after_primary_end_filter', false ); // // WPCS: XSS OK. ?>
    </div><!-- #primary -->

<?php
get_footer();
