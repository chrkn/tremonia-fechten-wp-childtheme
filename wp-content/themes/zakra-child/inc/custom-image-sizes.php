<?php
// custom image sizes

// medium double pixel size
add_image_size( 'tf_staff-gallery-medium-2x-4x3', 744, 558);
add_image_size( 'tf_staff-gallery-medium-2x-3x4', 744, 992);
add_image_size( 'tf_staff-gallery-medium-2x-1x1', 740, 740);

// medium normal pixel size
add_image_size( 'tf_staff-gallery-medium-4x3', 372, 279);
add_image_size( 'tf_staff-gallery-medium-3x4', 372, 496);
add_image_size( 'tf_staff-gallery-medium-1x1', 370, 370);

// small double pixel size
add_image_size( 'tf_staff-gallery-small-2x-4x3', 384, 288);
add_image_size( 'tf_staff-gallery-small-2x-3x4', 384, 512);
add_image_size( 'tf_staff-gallery-small-2x-1x1', 366, 366);

// small normal pixel size
add_image_size( 'tf_staff-gallery-small-4x3', 192, 144);
add_image_size( 'tf_staff-gallery-small-3x4', 192, 256);
add_image_size( 'tf_staff-gallery-small-1x1', 183, 183);

// flip card image
add_image_size( 'tf_staff-flip-card-thumbnail-5x3', 250, 150);
add_image_size( 'tf_staff-flip-card-small-5x3', 500, 300);
add_image_size( 'tf_staff-flip-card-large-5x3', 1000, 600);

// important notice image
add_image_size( 'tf_notice-1x1-small', 50, 50);
add_image_size( 'tf_notice-1x1-large', 100, 100);

// fechtschule sponsor image
add_image_size( 'tf_fechtschule-sponsor-small', 0, 80);
add_image_size( 'tf_fechtschule-sponsor-large', 0, 160);

// fechtschule highlight image
add_image_size( 'tf_fechtschule-highlight-small', 752);
add_image_size( 'tf_fechtschule-highlight-large', 1504);
add_image_size( 'tf_fechtschule-highlight-overview-small', 500, 250);
add_image_size( 'tf_fechtschule-highlight-overview-large', 1000, 500);

// fechtschule location image
add_image_size( 'tf_fechtschule-location-small', 300);
add_image_size( 'tf_fechtschule-location-large', 600);

// fechtschule instructor image
add_image_size( 'tf_fechtschule_instructor_small', 240);
add_image_size( 'tf_fechtschule_instructor_large', 480);