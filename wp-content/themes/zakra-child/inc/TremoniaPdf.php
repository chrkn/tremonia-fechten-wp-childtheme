<?php
class TremoniaPdf extends TCPDF
{
    public function Header(): void {
        // Logo
        $image_file = getcwd() . DIRECTORY_SEPARATOR . 'img'. DIRECTORY_SEPARATOR . 'tremonia_logo_bw.svg';
        $this->ImageSVG($image_file, 15, 10, '', 30);

        $this->SetFont('Georgia', '', 24);
        $this->setColor('text', 0, 0, 0);
        $this->writeHTMLCell(0, 0, 55, 8.25, $this->header_title);

    }
}