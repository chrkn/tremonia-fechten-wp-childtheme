<?php
/*
 * File for functions that overwrite main theme functions
 *
 */


/**
 * Original function in wp-content/themes/zakra/inc/hooks/header.php
 *
 * Used to deliver a different menu if a page/post is flagged as english.
 *
 */
function zakra_primary_menu() {
    // Bail out if the menu is disabled from customizer.
    if ( true === get_theme_mod( 'zakra_primary_menu_disabled', false ) ) {
        return;
    }
    ?>
    <nav id="site-navigation"
         class="<?php zakra_css_class( 'zakra_nav_class' ); ?> <?php zakra_primary_menu_class(); ?>">
        <?php
        if (get_field('language') == 'en' && !is_archive())
            $menu = 'main-english';
        else
            $menu = 'main';

        // make fechtschule-related changes to the navigation menu
        add_filter('wp_nav_menu_objects', 'ad_filter_menu', 10, 2);
        function ad_filter_menu($sorted_menu_objects, $args) {
//            echo '<pre>'; var_dump(get_fields()); echo '</pre>';

            if ($args->theme_location != 'menu-primary')
                return $sorted_menu_objects;

            // remove menu items if registration is not open
            foreach ($sorted_menu_objects as $key => $menu_object) {
                $registration_open = is_registration_open();
                // can also check for $menu_object->url for example
                if (!$registration_open) {
                    switch ($menu_object->title) {
                        case 'Anmeldung':
                        case 'Registration <span>en</span>':
                            unset($sorted_menu_objects[$key]);
                    }
                }

                // see all properties to test against:
//                 echo '<pre>';print_r($menu_object); echo '</pre>';
            }
//            echo '<pre>'; var_dump($sorted_menu_objects); echo '</pre>';
            return $sorted_menu_objects;
        }

        wp_nav_menu(
            array(
                'menu'            => $menu,
                'theme_location'  => 'menu-primary',
                'menu_id'         => 'primary-menu',
                'menu_class'      => 'menu-primary',
                'container_class' => 'menu',
                'fallback_cb'     => 'zakra_menu_fallback',
            )
        );
        ?>
    </nav><!-- #site-navigation -->
    <?php
}

/**
 * Original function in wp-content/themes/zakra/inc/hooks/header.php
 *
 * Used to deliver a different mobile menu if a page/post is flagged as english.
 *
 */
function zakra_mobile_navigation() {
    ?>
    <nav id="mobile-navigation" class="<?php zakra_css_class( 'zakra_mobile_nav_class' ); ?>"
        <?php echo wp_kses_post( apply_filters( 'zakra_nav_data_attrs', '' ) ); ?>>

        <?php
        if (get_field('language') == 'en')
            $menu_mobile = 'main-english';
        else
            $menu_mobile = 'main';
        wp_nav_menu(
            array(
                'menu'              => $menu_mobile,
                'theme_location'    => 'menu-primary',
                'menu_id'           => 'mobile-primary-menu',
            )
        );
        ?>
    </nav><!-- /#mobile-navigation-->
    <?php
}

/**
 * Original function in wp-content/themes/zakra/inc/template-tags.php
 *
 * Normally prints the author of a post: deactivated
 *
 */
function zakra_posted_by() {

}

/**
 * Page header: Corona overwrite todo: make permanent as fields from front page for important announcements.
 */
function zakra_page_header() {

    $page_header_meta = get_post_meta( zakra_get_post_id(), 'zakra_page_header' );

    // Return, if page header is disabled from customizer.
    if ( ( 'page-header' !== zakra_is_page_title_enabled() && ! zakra_is_breadcrumbs_enabled() ) || ( isset( $page_header_meta[0] ) && ! $page_header_meta[0] ) || is_front_page() ) {
        return;
    }

    $allowed_markup = array( 'h1', 'h2', 'h3', 'h3', 'h4', 'h5', 'h6', 'span', 'p', 'div' );
    $markup         = get_theme_mod( 'zakra_page_title_markup', 'h1' );
    $style          = apply_filters( 'zakra_page_title_align_filter', get_theme_mod( 'zakra_page_title_alignment', 'tg-page-header--left' ) );

    // If the markup doesn't match the allowed one set default one.
    if ( ! in_array( $markup, $allowed_markup, true ) ) {
        $markup = 'h1';
    }

    // Finale.
    $markup = apply_filters( 'zakra_page_header_markup', $markup );

    do_action( 'zakra_before_page_header' );

    if ( zakra_is_woocommerce_active() && function_exists( 'is_woocommerce' ) && is_woocommerce() && 'content-area' === get_theme_mod( 'zakra_page_title_enabled', 'page-header' ) ) {
        return;
    }
    ?>

    <header class="tg-page-header <?php echo esc_attr( $style ); ?>">
        <div class="<?php zakra_css_class( 'zakra_page_header_container_class' ); ?>">
            <?php
            if ( 'page-header' === get_theme_mod( 'zakra_page_title_enabled', 'page-header' ) ) {
                $page_title = zakra_get_title();

                // Page header title.
                echo sprintf(
                    '<%1$s class="tg-page-header__title">%2$s</%1$s>',
                    esc_attr( $markup ),
                    wp_kses_post( $page_title )
                );
            }
            ?>

            <?php
            // Page header breadcrumb.
            if ( function_exists( 'breadcrumb_trail' ) && zakra_is_breadcrumbs_enabled() ) {

                // Use WooCommerce breadcrumb.
                if ( zakra_is_woocommerce_active() && function_exists( 'is_woocommerce' ) && is_woocommerce() ) {

                    // Show WC breadcrumb on page header.
                    if ( 'page-header' === get_theme_mod( 'zakra_page_title_enabled', 'page-header' ) ) {

                        // Remove Breadcrumb from content.
                        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

                        // Make WC breadcrumb with the theme.
                        woocommerce_breadcrumb(
                            array(
                                'wrap_before' => '<nav role="navigation" aria-label="' . esc_html__( 'Breadcrumbs', 'zakra' ) . '" class="breadcrumb-trail breadcrumbs"><ul class="trail-items">',
                                'wrap_after'  => '</ul></nav>',
                                'before'      => '<li class="trail-item">',
                                'after'       => '</li>',
                                'delimiter'   => '',
                            )
                        );
                    }
                } else { // Theme breadcrumb.
                    /**
                     * Hook - zakra_action_breadcrumbs
                     *
                     * @hooked zakra_breadcrumbs - 10
                     */
                    do_action( 'zakra_action_breadcrumbs' );
                }
            }
            ?>
        </div>
    </header>
    <?php get_template_part( 'template-parts/content', 'aside-announcement' ); ?>
    <!-- /.page-header -->
    <?php
    do_action( 'zakra_after_page_header' );
}

/**
 * Header ends.
 */
function zakra_header_end() {
    ?>
    </header><!-- #masthead -->
    <?php
    if (is_front_page()):
        get_template_part( 'template-parts/content', 'aside-announcement' );
    endif;
    ?>
    <?php
}
