<?php

/**
 * Change html lang attribute, if page is flagged as english.
 *
 * @return void
 */
function handle_html_language_att() {
    if (get_field('language') == 'en') {
        function language_attribute($lang): string {
            return 'lang="en-GB"';
        }
        add_filter('language_attributes', 'language_attribute');
    }
}

function get_fechtschule_instructors_by_seminar ($data): array {
    $course_data = [];

    // iterate through the program information
    foreach ($data as $key => $day) {
        if (is_array($day) && strpos($key, 'programm_') === 0) {
            foreach ($day as $seminar) {
                $seminar_acf = get_fields($seminar);
                if (!empty($seminar_acf['dozent'])) {
                    foreach ($seminar_acf['dozent'] as $l => $instructor) {
                        $course_data['instructors'][$instructor][$seminar] = $seminar_acf;
                        $course_data['courses'][$seminar][$instructor] = get_fields($instructor);
                    }
                }
            }
        }
    }

    return $course_data;
}

function generate_instructor_list($courses, $is_english): string {
    $instructor_entries = [];
    /*
     * get the course - instructor pairings
     * they are stored as
     * instructorId,instructorId_courseId,courseId in an array
     */
    if (!empty($courses)) {
        foreach ($courses['instructors'] as $instructor_id => $course) {
            foreach ($course as $course_id => $course_item) {
                // case 1: 1 instructor has 1 course
                if (count($courses['courses'][$course_id]) == 1 && count($courses['instructors'][$instructor_id]) == 1) {
                    $instructor_course = $instructor_id . '_' . $course_id;
                    if (!in_array($instructor_course, $instructor_entries)) {
                        $instructor_entries[] = $instructor_course;
                    }
                }
                // case 2: 1 instructor has multiple courses
                elseif (count($courses['courses'][$course_id]) == 1 && count($courses['instructors'][$instructor_id]) > 1) {
                    $course_keys = '';
                    foreach ($courses['instructors'][$instructor_id] as $key => $inst_course) {
                        if ($course_keys != '') {
                            $course_keys .= ',' . $key;
                        } else {
                            $course_keys .= $key;
                        }
                    }
                    $instructor_courses = $instructor_id . '_' . $course_keys;
                    if (!in_array($instructor_courses, $instructor_entries)) {
                        $instructor_entries[] = $instructor_courses;
                    }
                }
                // case 3: multiple instructors have 1 course
                elseif (count($courses['courses'][$course_id]) > 1 && count($courses['instructors'][$instructor_id]) == 1) {
                    $instructor_keys ='';
                    foreach ($courses['courses'][$course_id] as $key => $course_inst) {
                        if ($instructor_keys != '') {
                            $instructor_keys .= ',' . $key;
                        } else {
                            $instructor_keys .= $key;
                        }
                    }
                    $instructors_course = $instructor_keys . '_' . $course_id;
                    if (!in_array($instructors_course, $instructor_entries)) {
                        $instructor_entries[] = $instructors_course;
                    }

                }
                // case 4: multiple instructors have multiple courses.
                elseif (count($courses['courses'][$course_id]) > 1 && count($courses['instructors'][$instructor_id]) > 1) {
                    $course_keys = '';
                    foreach ($courses['instructors'][$instructor_id] as $key => $inst_course) {
                        if ($course_keys != '') {
                            $course_keys .= ',' . $key;
                        } else {
                            $course_keys .= $key;
                        }
                    }

                    $instructor_keys ='';
                    foreach ($courses['courses'][$course_id] as $key => $course_inst) {
                        if ($instructor_keys != '') {
                            $instructor_keys .= ',' . $key;
                        } else {
                            $instructor_keys .= $key;
                        }
                    }

                    $instructors_course = $instructor_keys . '_' . $course_keys;
                    if (!in_array($instructors_course, $instructor_entries)) {
                        $instructor_entries[] = $instructors_course;
                    }
                }
            }
        }
    }

    $html = '<div class="tf_accordion">';
    for ($i = 0; $i < count($instructor_entries); $i++) {

        $data = explode('_', $instructor_entries[$i]);
        $instructors = explode(',', $data[0]);
        $courses = explode(',', $data[1]);
        $heading = '';
        $instructor_description = '';
        $course_description = '';

        $count = 0;
        foreach ($instructors as $key => $instructor) {
            $instructor = get_fields($instructor);

            // build the heading for the accordion element
            $heading = trim($heading) . '<b>';
            $heading = trim($heading) . $instructor['titel'] ?: '';
            $heading = trim($heading) . ' ' . $instructor['vorname'] ?: '';
            $heading = trim($heading) . ' ' . $instructor['nachname'] ?: '';
            $heading = trim($heading) . '</b>';
            $heading = trim($heading) . ($instructor['verein'] && $instructor['webseite'] ?
            ' (<a href="' . $instructor['webseite'] . '" target="_blank">' . $instructor['verein'] . '</a>)' :
                ($instructor['verein'] ? ' (' . $instructor['verein'] . ')' : ''));
            $heading = trim($heading) . ', ';

            // build the instructor
            if (!empty($instructor['foto'])) {
                if ($count > 0) {
                    $instructor_description .= '<div style="clear: both;"></div>';
                }
                $attr = [
                    'src'    => $instructor['foto']['url'],
                    'srcset' => $instructor['foto']['sizes']['tf_fechtschule_instructor_small'] . ', ' . $instructor['foto']['sizes']['tf_fechtschule_instructor_large'] . ' 2x',
                    'class'  => 'alignleft tf_fechtschule_image'
                ];
                $instructor_description .= ($instructor['foto'] ? wp_get_attachment_image($instructor['foto']['ID'], false, false, $attr) : '');
            }

            if ($is_english) {
                $instructor_description .= $instructor['vorstellung_en'] ?: '';
            } else {
                $instructor_description .= $instructor['vorstellung'] ?: '';
            }

            $count++;
        }

        // build the courses
        $topic = [];
        foreach ($courses as $course) {
            $course = get_fields($course);
            $type = '';
            if ($is_english) {
                if (!empty($course['art'])) {
                    $name_en = get_field('name_en', 'tf_fs_course_type' . '_' . $course['art']->term_id);
                    $type = ' (' . ($name_en ?: $course['art']->name) . ')';
                }
                if (!empty($course['waffengattung'])) {
                    $name_en = get_field('name_en', 'tf_fs_course_weapon' . '_' . $course['waffengattung']->term_id);
                    if (!in_array($name_en ?: $course['waffengattung']->name, $topic))
                        $topic[] = $name_en ?: $course['waffengattung']->name;
                }
                // add heading and description
                $course_description .=  ($course['titel_en'] && $course['inhalt_en'] ?
                        '<h5>' . $course['titel_en'] . $type . '</h5>': '');
                $course_description .= $course['inhalt_en'] ?: '';
                // add image gallery link if it exists
                if (!empty($course['link_zum_fotoalbum'])) {
                    $course_description .= '<p>' . "<a href=\"{$course['link_zum_fotoalbum']}\" target=\"_blank\" rel=\"noopener noreferrer\">Photo gallery on Facebook</a>" . '</p>';
                }
            } else {
                if (!empty($course['art'])) {
                    $type = ' (' . $course['art']->name . ')';
                }
                if (!empty($course['waffengattung']) && !in_array($course['waffengattung']->name, $topic)) {
                    $topic[] = $course['waffengattung']->name;
                }
                // add heading and description
                $course_description .= ($course['titel'] && $course['inhalt'] ?
                        '<h5>' . $course['titel'] . $type . '</h5>': '');
                $course_description .= $course['inhalt'] ?: '';
                // add image gallery link if it exists
                if (!empty($course['link_zum_fotoalbum'])) {
                    $course_description .= '<p>' . "<a href=\"{$course['link_zum_fotoalbum']}\" target=\"_blank\" rel=\"noopener noreferrer\">Bildergalerie auf Facebook</a>" . '</p>';
                }
            }

        }

        $is_checkbox = $instructor_description || $course_description || $instructor['foto'];
        $html .= '<div class="tf_accordion_item">';
        //todo first open element does not work if first is empty, need to use first element with content.
        $html .= '<input type="' . ($is_checkbox ? 'checkbox' : 'hidden') . '" id="instructor-' . $i . '"' . ($i == 0 ? ' checked' : '') . ' >';
        $html .= '<label for="instructor-' . $i . '"><h4>' . substr($heading, 0, -2) . ($topic ? ': ' .
                implode(', ', $topic) : '') .
            ($is_checkbox ? ' <i class="ea-expand-icon"></i>' : '') . '</h4></label>';
        $html .= '<div class="tf_accordion_content">';
        $html .= '<div class="tf_accordion_padding">';
        $html .= '<p>' . $instructor_description . '</p>';
        $html .= $course_description;
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
    }
    $html .= '</div>';

    return $html;
}

function date_formatter($date, $format, $language): string
{
    $timezone = new DateTimeZone('Europe/Berlin');
    try {
        $date = new DateTime($date, $timezone);
    }
    catch (Exception $e) {
        error_log($e->getMessage(), 'string');
        exit(1);
    }
    $date_format = new IntlDateFormatter(
                $language,
        IntlDateFormatter::NONE,
        IntlDateFormatter::NONE,
        'Europe/Berlin',
        IntlDateFormatter::TRADITIONAL,
                $format);

    return $date_format->format($date->getTimestamp());
}


function generate_programme($program, $is_english) {

    foreach ($program['programm_tag1'] as $day1) {
        $day1 = get_fields($day1);
        $program_data[$program['datum_tag1']][$day1['beginn']][] = $day1;
    }
    foreach ($program['programm_tag2'] as $day2) {
        $day2 = get_fields($day2);
        $program_data[$program['datum_tag2']][$day2['beginn']][] = $day2;
    }
    foreach ($program['programm_tag3'] as $day3) {
        $day3 = get_fields($day3);
        $program_data[$program['datum_tag3']][$day3['beginn']][] = $day3;
    }

    echo  '<div class="tf_program_tabs">';
    $count = 0;

    foreach ($program_data as $date => $programme) {
        $count++;
        echo '<input type="radio" name="tabset" id="tab' . $count . '" aria-controls="';
        echo date_formatter($date, 'EEEE', $is_english ? 'en_GB' : 'de_DE');
        if ($count == 1) {
            echo '" checked><label for="tab' . $count . '">';
        } else {
            echo '"><label for="tab' . $count . '">';
        }

        echo date_formatter($date, 'EEEE', $is_english ? 'en_GB' : 'de_DE');
        echo '</label>';
    }
    echo '<div class="tab-panels">';
    foreach ($program_data as $date => $programme) {
        echo '<section id="';
        echo date_formatter($date, 'EEEE', $is_english ? 'en_GB' : 'de_DE');
        echo '" class="tab-panel">';
        echo '<table>';
        foreach ($programme as $time => $items) {
            echo '<tr>';
            echo '<td>';
            echo $is_english ?
                date_formatter($time, "h:mm'&nbsp;'a", 'en_GB') :
                date_formatter($time, 'H:mm', 'de_DE');
            echo '</td>';
            echo '<td>';
            foreach ($items as $item) {
                // get instructors if they exist
                echo '<div>';
                $instructor_string = '';
                if ($item['dozent']) {
                    $instructor_string .= '(';
                    foreach ($item['dozent'] as $instructor) {
                        $instructor = get_fields($instructor);
                        $instructor_string .= trim($instructor['titel'] . ' ' . $instructor['vorname'] . ' ' .
                            $instructor['nachname']) . ', ';
                    }
                    $instructor_string = substr($instructor_string, 0, -2);
                    $instructor_string .= ')';
                } elseif (is_object($item['art']) && $item['art']->name != 'Programmpunkt') {
                    $instructor_string = ' (<abbr title="to be announced">TBA</abbr>)';
                }

                // get type of workshop if it exists and is not programmpunkt
                $type_string = '';
                if ($item['art'] && $item['art']->name != 'Programmpunkt') {
//                    echo '<pre>';var_dump($item['art']);echo '</pre>';
                    $type_en = get_field('name_en', 'tf_fs_course_type' . '_' . $item['art']->term_id);
                    $type_string = $is_english && $type_en ?
                        $type_en :
                        $item['art']->name;
                }

                echo ($type_string ? '<b>' . $type_string . '</b><br><i>' : '') . str_Replace('<br>', ' ',$is_english ? $item['titel_en'] : $item['titel']) . ($type_string ? '</i> ' : '') .
                    $instructor_string . '<br>';
                echo '</div>';
            }
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '</section>';
    }

    echo '</div></div>';

}

function is_registration_open(): bool {
    // get the latest fechtschule
    $latest_fechtschule = get_posts([
        'post_type'     => 'tf_fechtschule',
        'post_status'   => 'publish',
        'numberposts'   => -1,
        'orderby'       => 'date',
		'order'         => 'DESC'
    ]);
    $latest_fechtschule_acf = [];
    $registration_open      = false;
    $registration_overwrite = false;

    foreach ($latest_fechtschule as $fechtschule) {
        $fechtschule_acf = get_fields($fechtschule->ID);
        if (!empty($fechtschule_acf['art_des_beitrags']) && $fechtschule_acf['art_des_beitrags'] == 'fechtschule') {
            $latest_fechtschule_acf = get_fields($fechtschule->ID);
            break;
        }
    }

    // check registration status
    if (!empty($latest_fechtschule_acf['registrierung'])
            && !empty($latest_fechtschule_acf['registrierung']['reg_beginn'])) {
        $now    = new DateTimeImmutable('now', new DateTimeZone('Europe/Berlin'));
        $begin  = new DateTimeImmutable($latest_fechtschule_acf['registrierung']['reg_beginn'],
            new DateTimeZone('Europe/Berlin'));
        // if no end is provided use current date + 3 months, basically infinite registration
        if (empty($latest_fechtschule_acf['registrierung']['reg_ende'])) {
            $end = new DateTime('now', new DateTimeZone('Europe/Berlin'));
            $end->modify('+3 months');
        } else {
            $end = new DateTimeImmutable($latest_fechtschule_acf['registrierung']['reg_ende'],
                new DateTimeZone('Europe/Berlin'));
        }

        $registration_open = $now > $begin && $now < $end;
    }

    // check registration overwrite for staff/instructors
    if (!empty($latest_fechtschule_acf['registrierung']['registrierungscode'])) {
        $query_var = get_query_var('register');

        $registration_overwrite = $query_var === $latest_fechtschule_acf['registrierung']['registrierungscode'];
    }

    return $registration_open || $registration_overwrite;
}

function tf_get_fechtschule_acf_content($post_id): array {
    $content = get_fields($post_id);

    switch ($content['art_des_beitrags']) {
        case 'translation':
            $content = [
                'language' => 'en',
                'content' => get_fields($content['translation']->ID)
            ];
            break;
        case 'fechtschule':
            $content = [
                'language' => 'de',
                'content' => $content
            ];
            break;
    }

    return is_array($content) ? $content : [];
}