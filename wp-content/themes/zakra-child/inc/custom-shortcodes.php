<?php
/*
 * File for all custom shortcodes
 *
 */


/* * * * * * * * * * * * * * * * * *
 *  For the staff custom post type *
 * * * * * * * * * * * * * * * * * */

/**
 * Shortcode to display an image on the staff profile page, chosen from the gallery or main image
 *
 * @param $atts
 * @return false|string
 */
function staff_picture($atts) {
    if (get_post_type() == 'tf_staff') {
        $a = shortcode_atts( [
            'src'   => '',
            'align' => ''
        ], $atts );
        $a['align'] = $a['align'] != 'right' ? 'left' : 'right';
        if ($a['src'] == '') {
            $image_id = get_field('bild', get_the_ID())['ID'];
            $image_large = wp_get_attachment_image_src($image_id, 'tf_staff-flip-card-large-5x3')[0];
            $image_small = wp_get_attachment_image_src($image_id, 'tf_staff-flip-card-small-5x3')[0];
            $attr = [
                'class'  => 'tf_staff-single-img',
                'src'    => $image_large,
                'srcset' => $image_small . ' 1x, ' . $image_large . ' 2x'
            ];
            $image =
                "<div class=\"tf_staff-single-img-container align{$a['align']}\">" .
                wp_get_attachment_image($image_id, [500, 300], false, $attr) .
                '</div>';
        }
        elseif ($image_id = get_field('bildergalerie', get_the_ID())[$a['src']]['ID']) {
            switch ($a['src']) {
                case 'foto_1':
                case 'foto_2':
                case 'foto_5':
                case 'foto_6':
                    $image_ratio = '4x3';
                    break;
                case 'foto_3':
                case 'foto_4':
                case 'foto_7':
                case 'foto_8':
                    $image_ratio = '3x4';
                    break;
                default:
                    $image_ratio = '1x1';
            }
            $image_large = wp_get_attachment_image_src($image_id, 'tf_staff-gallery-medium-2x-' . $image_ratio);
            $image_small = wp_get_attachment_image_src($image_id, 'tf_staff-gallery-medium-' . $image_ratio);
            $attr = [
                'class'  => 'tf_staff-single-gallery-img align' . $a['align'],
                'src'    => $image_large[0],
                'srcset' => $image_small[0] . ' 1x, ' . $image_large[0] . ' 2x'
            ];
            $image = wp_get_attachment_image($image_id, [$image_large[1], $image_large[2]], false, $attr);
        }
        else {
            return false;
        }
        return $image;
    }
    return false;
}
add_shortcode( 'foto', 'staff_picture' );


/**
 * Shortcode to display the quote on the staff single view page
 *
 * @param $atts
 * @return false|string
 */
function staff_quote($atts) {
    $quote = get_field('zitat', get_the_ID());
    if (get_post_type() == 'tf_staff' && $quote['text']) {
        $a = shortcode_atts([
            'align' => ''
        ], $atts);
        switch ($a['align']) {
            case 'right':
            case 'left':
            case 'flex':
                // attribute has been used correctly
                break;
            default:
                // attribute has not been used correctly; set to default
                $a['align'] = 'right';
        }
        $text = nl2br($quote['text']);
        $text = str_replace(['^|', '|$'], ['<span class="quote-highlight-color">', '</span>'], $text);
        $author = $quote['autor'] ?: get_the_title(get_the_ID());
        return "<div class=\"tf_staff-quote align{$a['align']}\">
                  <blockquote>
                    <p>$text</p>
                    <cite>$author</cite>
                  </blockquote>
                </div>";
    }
    return false;
}
add_shortcode( 'zitat', 'staff_quote' );

function direct_contact($atts, $content = '') {
    $attributes =
        shortcode_atts( [
            'who'   => !empty($atts['who']) ? $atts['who'] : '',
            'why'   => !empty($atts['why']) ? $atts['why'] : '',
            'text'  => $content ?: 'Link'
        ], $atts );

    return '<a href="/kontakt/?your-contact=' . $attributes['who'] . '&your-subject=' . $attributes['why'] . '">' . $attributes['text'] . '</a>';
}
add_shortcode( 'kontakt', 'direct_contact' );
