<?php

// get wordpress & plugin default functions, as well as TCPDF files
const WP_USE_THEMES = false;
require_once '../../../wp-load.php';
require_once 'external/TCPDF-main/tcpdf_import.php';
require_once 'inc/TremoniaPdf.php';

$post_id = !empty($_POST['post_id']) ? (int) sanitize_text_field($_POST['post_id']) : 0;
$content = tf_get_fechtschule_acf_content($post_id);
$is_english = $content['language'] == 'en';
$current_date = '';
$pdf_data = [];

if (is_array($content['content']['programm'])) {
    foreach (array_filter($content['content']['programm']) as $key => $event_date) {
        if (str_starts_with($key, 'datum_')) {
            $current_date = $event_date;
            $pdf_data[$current_date] = [
                'date_de' => date_formatter($current_date, 'EEEE, d. MMMM y', 'de_DE'),
                'date_en' => date_formatter($current_date, 'EEEE, d MMMM y', 'en_GB'),
                'timeslots' => []
            ];
        }
        if (str_starts_with($key, 'programm_')) {
            foreach ($event_date as $program_item) {
                $program_item_content = get_fields($program_item);

                if ($program_item_content) {
                    // extract type information
                    if (!empty($program_item_content['art']) && is_object($program_item_content['art'])) {
                        $type_name_en = get_field('name_en', 'tf_fs_course_type_' . $program_item_content['art']->term_id);
                        $type = [
                            'de' => $program_item_content['art']->name,
                            'en' => !empty($type_name_en)
                                ? $type_name_en
                                : $program_item_content['art']->name,
                            'slug' => $program_item_content['art']->slug
                        ];
                    }

                    // extract the instructors
                    $instructors = [];
                    if (!empty($program_item_content['dozent'])) {
                        foreach ($program_item_content['dozent'] as $instructor) {
                            $instructor = get_fields($instructor);
                            $instructors[] = [
                                'name' => trim((!empty($instructor['titel']) ? $instructor['titel'] . ' ' : '')
                                    . (!empty($instructor['vorname']) ? $instructor['vorname'] . ' ' : '')
                                    . (!empty($instructor['nachname']) ? $instructor['nachname'] . ' ' : '')),
                                'club' => !empty($instructor['verein']) ? $instructor['verein'] : ''
                            ];
                        }
                    }

                    // build the pdf data
                    if (empty($pdf_data[$current_date]['timeslots'][$program_item_content['beginn']])) {
                        $pdf_data[$current_date]['timeslots'][$program_item_content['beginn']] = [
                            'time_de' => date_formatter($program_item_content['beginn'], 'H:mm', 'de_DE'),
                            'time_en' => date_formatter($program_item_content['beginn'], 'h:mm\'&nbsp;\'a', 'en_GB'),
                            'program_items' => []
                        ];
                    }
                    $pdf_data[$current_date]['timeslots'][$program_item_content['beginn']]['program_items'][] = [
                        'type' => $type ?? null,
                        'title' => [
                            'de' => $program_item_content['titel'] ?: null,
                            'en' => $program_item_content['titel_en'] ?: null
                        ],
                        'description' => [
                            'de' => !empty($program_item_content['inhalt'])
                                ? $program_item_content['inhalt']
                                : null,
                            'en' => !empty($program_item_content['inhalt_en'])
                                ? $program_item_content['inhalt_en']
                                : null
                        ],
                        'instructors' => $instructors ?? null
                    ];
                }
            }
        }
    }
}

//echo '<pre>';var_dump($pdf_data);echo '</pre>';exit;

// add font to tcpdf
$font_path = getcwd() . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'fonts' . DIRECTORY_SEPARATOR;
TCPDF_FONTS::addTTFfont($font_path . 'Georgia.ttf', 'TrueTypeUnicode', '', 96);
TCPDF_FONTS::addTTFfont($font_path . 'georgia bold.ttf', 'TrueTypeUnicode', '', 96);
TCPDF_FONTS::addTTFfont($font_path . 'Georgia Regular font.ttf', 'TrueTypeUnicode', '', 96);
TCPDF_FONTS::addTTFfont($font_path . 'georgiab.ttf', 'TrueTypeUnicode', '', 96);

// if we have content for the program, create the PDF and start the download-stream
if (!empty($pdf_data)) {
    $pdf = new TremoniaPdf(
            PDF_PAGE_ORIENTATION,
            PDF_UNIT,
            PDF_PAGE_FORMAT,
            true,
            'UTF-8',
            false
        );

    // set font and color
    $pdf->SetFont('Georgia', '', 12, '', false);
    $pdf->setColor('text', 0, 0, 0);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Tremonia Fechten e.V.');
    $pdf->SetTitle('Programm Tremonia Fechtschule');

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, 50, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(3);

    // deactivate footer
    $pdf->setPrintFooter(false);

    $pdf->setHeaderData('', '',
        $is_english
            ? 'Tremonia Fencing School' . (array_key_first($pdf_data) ? ' ' . date_formatter(array_key_first($pdf_data), 'Y', 'en_GB') : '') . '<br />Programme'
            : 'Tremonia Fechtschule' . (array_key_first($pdf_data) ? ' ' . date_formatter(array_key_first($pdf_data), 'Y', 'de_DE') : '') . '<br />Programm');

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // reduce margin for p tags
    $tagvs =
        ['p' =>
            [
                0 => ['h' => 0, 'n' => 0],
                1 => ['h' => 1, 'n' => 3]
            ]
        ];
    $pdf->setHtmlVSpace($tagvs);

    foreach ($pdf_data as $date => $event_day) {
        $pdf->AddPage();
        $html = ($is_english ? $event_day['date_en'] : $event_day['date_de']);
        $pdf->setFontSize(16);
        $pdf->writeHTMLCell(0,0,15, $pdf->getY(), $html, '', 2,'', true,'L',true);
        $pdf->setY($pdf->getY() + 5);

        foreach ($event_day['timeslots'] as $timeslot) {
            $html = $is_english ? $timeslot['time_en'] : $timeslot['time_de'];
            $pdf->setFontSize(13);
            $pdf->writeHTMLCell(0,0,15,$pdf->getY() + 5, $html, '', 0,'', true,'L',true);

            foreach ($timeslot['program_items'] as $number => $program_item) {
                $pi_type = trim($is_english ? $program_item['type']['en'] : $program_item['type']['de']);
                $pi_title = trim($is_english ? $program_item['title']['en'] : $program_item['title']['de']);
                $pi_title = '<span style="font-weight:bolder;">'
                    . ($program_item['type']['slug'] !== 'programmpunkt' ? $pi_type . ': ' . $pi_title : $pi_title)
                    . '</span>';
                $pi_description = trim($is_english
                    ? $program_item['description']['en']
                    : $program_item['description']['de']);
                $pi_instructors = '';
                $y_mod = $number > 0 ? 5 : 0;

                foreach ($program_item['instructors'] as $key => $instructor) {
                    $pi_instructors .= $key > 0 ? ', ' : '';
                    $pi_instructors .= !empty($instructor['name']) ? $instructor['name'] : '';
                    $pi_instructors .= !empty($instructor['club']) ? ' (' . $instructor['club'] . ')' : '';
                }

                if (!empty($pi_title)) {
                    $pdf->setFontSize(13);
                    $pdf->writeHTMLCell(0,0,45,$pdf->getY() + $y_mod, $pi_title, '', 2,'', true,'L',true);
                }

                if ($pi_instructors !== '') {
                    $pdf->setFontSize(13);
                    $pdf->writeHTMLCell(0,0,45,$pdf->getY() + 1, $pi_instructors, '', 2,'', true,'L',true);
                }

                if ($pi_description !== '') {
                    $pdf->setFontSize(12);
                    $pdf->setCellHeightRatio(1.4);
                    $pdf->writeHTMLCell(0,0,45,$pdf->getY() + 3, $pi_description, '', 2,'', true,'L',true);
                }
            }
        }
    }
//exit;
    ob_end_clean();
    $pdf->Output($is_english ? 'Programme Fencing School.pdf' : 'Programm Fechtschule.pdf', 'D');
} else {
    // back we go, nothing to download
    $location = $post_id == 0 ? '/' : get_permalink($post_id);
    header("Location: $location", true, 307);
}
