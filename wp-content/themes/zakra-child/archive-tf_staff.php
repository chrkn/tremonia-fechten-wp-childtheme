<?php
/**
 * The template for displaying archive pages
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zakra
 */

function add_tf_staff_image_css() {
    $args = [
        'post_type'=> 'tf_staff',
        'order'    => 'DESC'
    ];

    ?><style><?php
    // check, if there is content, if so set the background-image for each element as header css
    $the_query = new WP_Query( $args );
    if($the_query->have_posts() ) :
        while ( $the_query->have_posts() ) :
            $the_query->the_post();
            $acf_data = get_fields();
            ?>
            #tf_staff-<?php the_ID() ?> .tf_staff-flip-card-front {background: #aea47d url('<?php echo $acf_data['bild']['sizes']['tf_staff-flip-card-small-5x3'] ?>') no-repeat right;background-size: 500px}
            @media only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 2/1),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx) {#tf_staff-<?php the_ID() ?> .tf_staff-flip-card-front {background: #aea47d url('<?php echo $acf_data['bild']['sizes']['tf_staff-flip-card-large-5x3'] ?>') no-repeat right;background-size: 500px}}
        <?php
        endwhile;
    endif;
    ?>
    /* remove padding on super small phones */
    @media screen and (max-width: 340px) {#content > .tg-container {padding-left: 0;padding-right: 0}
}</style><?php
}
add_filter('wp_head', 'add_tf_staff_image_css');

get_header();
?>

    <div id="primary" class="content-area primary-full-width tf_staff-container">
		<?php echo apply_filters( 'zakra_after_primary_start_filter', false ); // WPCS: XSS OK. ?>

		<?php if ( have_posts() ) : ?>
			<?php
			do_action( 'zakra_before_posts_the_loop' );

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			do_action( 'zakra_after_posts_the_loop' );

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		<?php echo apply_filters( 'zakra_after_primary_end_filter', false ); // // WPCS: XSS OK. ?>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
