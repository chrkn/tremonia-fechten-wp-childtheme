<?php

/**
 * Template part for displaying the language switch
 *
 */

$translation_page = get_field('translation');

if (!empty($translation_page)):
    $translation_page_url = get_permalink($translation_page->ID);
    $language = get_field('language');
    $links = '';
    if ($language == 'de') {
        $links = '<li>Deutsch | </li>' . '<li><a title="In English, please!" hreflang="en" href="' . $translation_page_url . '">English</a></li>';
    }
    elseif ($language == 'en') {
        $links = '<li><a title="Auf Deutsch, bitte!" hreflang="de" href="' . $translation_page_url . '">Deutsch</a> | </li>' . '<li>English</li>';
    }
    ?>
    <nav id="language-switch">
        <ul>
            <?php echo $links; ?>
        </ul>
    </nav>
<?php
endif;
