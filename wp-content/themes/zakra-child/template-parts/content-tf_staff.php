<?php
/**
 * Template part for displaying posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zakra
 */

$meta_style = get_theme_mod( 'zakra_blog_archive_meta_style', 'tg-meta-style-one' );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $meta_style ); ?>>
    <?php
    $data_staff = get_fields();
    if ($data_staff['age-condition'] == 'Datum')
        $data_staff['alter'] = 'geburtsdatum';
    elseif ($data_staff['age-condition'] == 'Jahr')
        $data_staff['alter'] = 'geburtsjahr';
    ?>
    <div class="tf_staff-flip-card" id="tf_staff-<?php the_ID() ?>">
        <div class="tf_staff-flip-card-content">
            <div class="tf_staff-flip-card-front">
                <div class="tf_staff-flip-card-information">
                    <h2><?php echo $data_staff['vorname'] . ' ' .  $data_staff['nachname'] ?></h2>
                    <p class="subline"><?php echo $data_staff['aufgaben'] ?></p>
                </div>
                <div class="tf_staff-flip-card-icon">
                    <i class="fa fa-refresh"></i>
                </div>
            </div>
            <div class="tf_staff-flip-card-back">
                <div class="tf_staff-flip-card-information">
                    <h2><?php echo $data_staff['vorname'] . ' ' . $data_staff['weitere_vornamen'] . ' ' .  $data_staff['nachname'] ?></h2>
                    <p class="subline"><?php echo $data_staff['aufgaben'] ?></p>
                    <ul>
                        <?php if (!empty($data_staff['beruf'])): ?>
                            <li><?php echo $data_staff['beruf'] ?></li>
                        <?php endif; ?>
                        <?php if (!empty($data_staff['alter'])): ?>
                            <li>geb. <?php echo $data_staff[$data_staff['alter']] ?></li>
                        <?php endif; ?>
                        <?php if (!empty($data_staff['hfseit'])): ?>
                            <li><abbr title="Historisches Fechten">HF</abbr> seit <?php echo $data_staff['hfseit'] ?></li>
                        <?php endif; ?>
                        <?php if (!empty($data_staff['favorite1']) || !empty($data_staff['favorite2'])): ?>
                        <li>
                            Trainiert besonders gern:
                            <ul>
                                <?php if (!empty($data_staff['favorite1'])): ?>
                                    <li><?php echo $data_staff['favorite1'] ?></li>
                                <?php endif; ?>
                                <?php if (!empty($data_staff['favorite2'])): ?>
                                    <li><?php echo $data_staff['favorite2'] ?></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="tf_staff-flip-card-links">
                    <?php if (!empty($data_staff['email'])): ?>
                    <form method="post" action="/kontakt/" class="tf_staff-contact-form">
                        <input type="hidden" id="your-contact" name="your-contact" value="<?php echo $data_staff['email'] ?>">
                        <input type="submit" value="Kontakt" class="tf_staff-contact-submit">
                    </form>
                    <?php echo $data_staff['profile'] ? '| <a href="' . get_the_permalink() . '" class="tf_staff-flip-card-profile">Zum Profil</a>' : '' ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
