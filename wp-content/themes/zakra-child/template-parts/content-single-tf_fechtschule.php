<?php
/**
 * Template part for displaying posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zakra
 */

$content_orders = get_theme_mod(
    'zakra_single_post_content_structure', array(
        'featured_image',
        'title',
        'meta',
        'content',
    )
);

$meta_orders = get_theme_mod(
    'zakra_single_blog_post_meta_structure', array(
        'comments',
        'categories',
        'author',
        'date',
        'tags',
    )
);

$meta_style = get_theme_mod('zakra_blog_archive_meta_style', 'tg-meta-style-one');

// get advanced custom fields data
$acf_data = get_fields();
// check, if we are on the english version of the page and if so, get content from german page entry
if ($is_english = $acf_data['art_des_beitrags'] == 'translation')
    $acf_data = get_fields($acf_data['translation']->ID);
// get the location information
if ($acf_data['ort'])
    $acf_data['ort'] = get_fields($acf_data['ort']->ID);
// get the courses and instructors
$courses = get_fechtschule_instructors_by_seminar($acf_data['programm']);
?>

    <article id="post-<?php the_ID(); ?>" <?php post_class($meta_style); ?>>
        <?php // highlight image ?>
        <?php if ($acf_data['highlightimage_en'] || $acf_data['highlightimage']): ?>
            <?php
                $img = $is_english && $acf_data['highlightimage_en'] ? $acf_data['highlightimage_en'] : $acf_data['highlightimage'];
                $attr = [
                    'src'    => $img['url'],
                    'srcset' => $img['sizes']['tf_fechtschule-highlight-small'] . ', ' . $img['sizes']['tf_fechtschule-highlight-large'] . ' 2x'
                ];
            ?>
            <div class="post-thumbnail">
                <?php echo wp_get_attachment_image($img['ID'], false, false, $attr) ?>
            </div>
        <?php endif; ?>
        <?php // end highlight image ?>
        <?php do_action('zakra_before_single_post'); ?>

        <div class="entry-content tf_staff-single">
            <?php // introduction ?>
            <?php if ($acf_data['fechtschule_archivieren']): ?>
                <h2><?php echo $is_english ? 'Review' : 'Rückblick'; ?></h2>
            <?php endif; ?>

            <?php if ($is_english && $acf_data['einleitungstext_en']): ?>
                <?php echo $acf_data['einleitungstext_en'] ?>
            <?php elseif (!$is_english && $acf_data['einleitungstext']): ?>
                <?php echo $acf_data['einleitungstext'] ?>
            <?php endif; ?>
            <?php // end introduction ?>
            <?php if (!$acf_data['nur_einleitung']): ?>
                <?php // time and place ?>
                <?php if (!$acf_data['fechtschule_archivieren']): ?>
                <h3><?php echo $is_english ? 'Time and Place' : 'Zeit und Ort'; ?></h3>
                <?php if ($acf_data['ort']['bild']): ?>

                <?php
                    $attr = [
                        'src'    => $acf_data['ort']['bild']['url'],
                        'srcset' => $acf_data['ort']['bild']['sizes']['tf_fechtschule-location-small'] . ', ' . $acf_data['ort']['bild']['sizes']['tf_fechtschule-location-large'] . ' 2x',
                        'class'  => 'tf_fechtschule_location alignright'
                    ];
                    echo wp_get_attachment_image($acf_data['ort']['bild']['ID'], false, false, $attr)
                ?>
                <?php endif; ?>
                <?php if ($is_english): ?>
                    <p>This year's Fechtschule (fencing school) will begin
                        on <?php echo date_formatter($acf_data['beginn'], "EEEE, d MMMM 'at' K:mm a", 'en_GB'); ?> and will
                        end on <?php echo date_formatter($acf_data['ende'], "EEEE, d MMMM y 'around' K:mm a", 'en_GB') ?>.
                        </p>
                <?php else: ?>
                    <p>Die Fechtschule beginnt
                        am <?php echo date_formatter($acf_data['beginn'], "EEEE, 'dem' d. MMMM, 'um' H 'Uhr'", 'de_DE'); ?>
                        und endet
                        gegen <?php echo date_formatter($acf_data['ende'], "H 'Uhr' 'am' EEEE, 'dem' d. MMMM y", 'de_DE'); ?>.
                        </p>
                <?php endif; ?>

                <h4><?php echo $is_english ? 'The Location' : 'Die Location'; ?></h4>
                <p><?php echo $is_english && $acf_data['ort']['kurzbeschreibung_en'] ? $acf_data['ort']['kurzbeschreibung_en'] : ($acf_data['ort']['kurzbeschreibung'] && !$is_english ? $acf_data['ort']['kurzbeschreibung'] : ''); ?></p>
                <p>
                    <address>
                        <?php
                        $link = $is_english && $acf_data['ort']['url_en'] ? $acf_data['ort']['url_en'] : $acf_data['ort']['url'];
                        $location_name = $is_english && $acf_data['ort']['name_en'] ? $acf_data['ort']['name_en'] : $acf_data['ort']['name'];

                        echo '<a href="' . $link . '" target="_blank" rel="noopener noreferrer">' . $location_name . '</a><br>';
                        echo $is_english ?
                            $acf_data['ort']['adresse']['nr'] . ' ' . $acf_data['ort']['adresse']['strasse'] . '<br>' :
                            $acf_data['ort']['adresse']['strasse'] . ' ' . $acf_data['ort']['adresse']['nr'] . '<br>';
                        echo $is_english ?
                            $acf_data['ort']['adresse']['plz'] . ' ' . $acf_data['ort']['adresse']['ort'] :
                            $acf_data['ort']['adresse']['ort'] . ' ' . $acf_data['ort']['adresse']['plz'];
                        echo $is_english ? '<br>Germany' : '';
                        ?>
                    </address>
                </p>
            <?php endif; ?>
            <?php // end time and place ?>

            <?php // instructors ?>
            <?php if (!empty($courses)): ?>
                <h3><?php echo $is_english ? 'Instructors' : 'Dozenten' ?></h3>

                <?php echo generate_instructor_list($courses, $is_english); ?>
            <?php endif; ?>
            <?php // end instructors ?>

            <?php // programme ?>
            <?php if (!empty($acf_data['programm'])): ?>
                <h3><?php echo $is_english ? 'Programme' : 'Programm' ?></h3>

                <?php generate_programme($acf_data['programm'], $is_english); ?>
            <?php endif; ?>
            <?php // end programme ?>
            <?php endif; // !$acf_data['nur_einleitung'] ?>
        </div>
        <!-- .entry-content -->

        <?php do_action('zakra_after_single_post'); ?>
    </article><!-- #post-<?php the_ID(); ?> -->

<?php
