<?php
$acf_data = get_fields();
$language = !empty($acf_data['language']) ? $acf_data['language'] : 'de';

if ($acf_data['art_des_beitrags'] == 'static') {
    $posts = get_posts([
        'post_type' => 'tf_fechtschule',
        'post_status' => 'publish',
        'numberposts' => -1
    ]);

    foreach ($posts as $post) {
        // first one should always be the one the highest ID = latest, right? ... Right?
        $acf_data_current = get_fields($post->ID);
        if ($acf_data_current['art_des_beitrags'] == 'fechtschule') {
            $acf_data = get_fields($post->ID);
            $is_english = $language == 'en';
            break;
        }
    }
} else {
    if ($is_english = $acf_data['art_des_beitrags'] == 'translation') {
        $acf_data = get_fields($acf_data['translation']->ID);
    }
}

// get place data
if($acf_data['ort']) {
    $acf_data['ort'] = get_fields($acf_data['ort']->ID);
}
?>

<aside id="secondary" class="tg-site-sidebar widget-area ">
    <section class="widget tf_fechtschule-essential-information">
        <h2 class="widget-title">
            <?php echo $is_english ? 'When' : 'Wann'; ?>?
        </h2>
        <p>
            <?php echo $is_english ?
                date_formatter($acf_data['beginn'], "EEEE, d/M/yy", 'en_GB') :
                date_formatter($acf_data['beginn'], "EEEE, d.M.yy", 'de_DE') ?>
            &dash;
            <?php echo $is_english ?
                date_formatter($acf_data['ende'], "EEEE, d/M/yy", 'en_GB') :
                date_formatter($acf_data['ende'], "EEEE, d.M.yy", 'de_DE') ?>
        </p>
        <h2 class="widget-title">
            <?php echo $is_english ? 'Where' : 'Wo'; ?>?
        </h2>
        <address>
            <p>
            <?php
            $link = $is_english && $acf_data['ort']['url_en'] ?
                $acf_data['ort']['url_en'] :
                $acf_data['ort']['url'];
            $location_name = $is_english && $acf_data['ort']['name_en'] ?
                $acf_data['ort']['name_en'] :
                $acf_data['ort']['name'];

            echo '<a href="' . $link . '" target="_blank" rel="noopener noreferrer">' . $location_name. '</a><br>';
            echo $is_english ?
                $acf_data['ort']['adresse']['nr'] . ' ' . $acf_data['ort']['adresse']['strasse'] . '<br>' :
                $acf_data['ort']['adresse']['strasse'] . ' ' . $acf_data['ort']['adresse']['nr'] . '<br>';
            echo $is_english ?
                $acf_data['ort']['adresse']['plz'] . ' ' . $acf_data['ort']['adresse']['ort'] :
                $acf_data['ort']['adresse']['ort'] . ' ' . $acf_data['ort']['adresse']['plz'];
            echo $is_english ? '<br>Germany' : '';
            ?>
            </p>
        </address>
        <h2 class="widget-title">
            <?php echo $is_english ? 'How much' : 'Wieviel'; ?>?
        </h2>
        <p><?php echo $acf_data['preisspanne'] ?: 'tba' ?></p>
        <?php if ($acf_data['sponsoren']): ?>
            <h2 class="widget-title">
                <?php echo $is_english ? 'Sponsors' : 'Sponsoren'; ?>
            </h2>
            <div style="display: flex; flex-wrap: wrap; justify-content: flex-start;">
                <?php
                foreach ($acf_data['sponsoren'] as $sponsor):
                    $sponsor = get_fields($sponsor->ID);
                    ?>
                    <a target="_blank" rel="noreferrer noopener" title="<?php echo $is_english && $sponsor['anzeigename_en'] ? $sponsor['anzeigename_en'] : $sponsor['anzeigename'] ?>" href="<?php echo $is_english && $sponsor['url_en'] ? $sponsor['url_en'] : $sponsor['url'] ?>">
                        <img style="margin: 0 15px 15px 0;height:80px;width: auto;" src="<?php echo $is_english && $sponsor['logo_en'] ? $sponsor['logo_en']['url'] : $sponsor['logo']['url'] ?>" alt="<?php echo $is_english && $sponsor['anzeigename_en'] ? $sponsor['anzeigename_en'] : $sponsor['anzeigename'] ?>">
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <h2 class="widget-title">
            <?php echo $is_english ? 'Contact' : 'Kontakt'; ?>
        </h2>
        <p><a href="mailto:fechtschule@tremonia-fechten.de">fechtschule@tremonia-fechten.de</a></p>
        <?php // registration button; display only, if fs not archived and registration start is in the past ?>
        <?php if (!$acf_data['fechtschule_archivieren']
                && !empty($acf_data['registrierung']['reg_beginn'])
                && date($acf_data['registrierung']['reg_beginn']) <= date("Y/m/d h:i")): ?>
            <p>
                <?php
                // show registration button if not over yet
                if (empty($acf_data['registrierung']['reg_ende'])
                    || (!empty($acf_data['registrierung']['reg_ende'])
                        && date($acf_data['registrierung']['reg_ende']) >= date("Y/m/d h:i"))):
                ?>
                    <a href="<?php echo $is_english ? '/fechtschule/registration/' : '/fechtschule/anmeldung/'; ?>">
                        <button type="button">
                            <?php echo $is_english ? 'Register now' : 'Jetzt anmelden' ?>
                        </button>

                    </a>
                <?php else: ?>
                    <button type="button">
                        <?php echo $is_english ? 'Registration closed' : 'Anmeldung geschlossen' ?>
                    </button>
                <?php endif; ?>

            </p>
        <?php endif; ?>
        <?php if (!empty($acf_data['programm']['programm_download'])): ?>
        <form action="/wp-content/themes/zakra-child/download.php" method="post">
            <input type="hidden" value="<?php the_ID() ?>" name="post_id">
            <input type="submit" value=" <?php echo $is_english ? 'Programme PDF' : 'Programm-PDF' ?>">
        </form>
        <?php endif; ?>
    </section>
</aside>