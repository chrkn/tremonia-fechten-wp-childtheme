<?php
$acf_data = get_fields(get_option('page_on_front'));
$language = get_field('language');
?>
<?php if ($acf_data['announcement_active'] && $language != 'en'): ?>
<aside id="tf_announcements">
    <div class="tf_announcements_content">
        <h3><?php echo $acf_data['announcement_heading'] ?></h3>
        <?php if (!empty($acf_data['announcement_image'])): ?>
        <?php
            $attr = [
                'class'  => 'tf_announcements_image_left',
                'src'    => $acf_data['announcement_image']['sizes']['tf_notice-1x1-large'],
                'srcset' => $acf_data['announcement_image']['sizes']['tf_notice-1x1-small'] . ' 1x, ' . $acf_data['announcement_image']['sizes']['tf_notice-1x1-large'] . ' 2x'
            ];
            echo wp_get_attachment_image($acf_data['announcement_image']['ID'], [50,50], false, $attr);
            $attr['class'] = 'tf_announcements_image_right';
            echo wp_get_attachment_image($acf_data['announcement_image']['ID'], [50,50], false, $attr) ?>
        <?php endif; ?>
        <?php echo $acf_data['announcement_text'] ?>
    </div>
</aside>
<?php endif;