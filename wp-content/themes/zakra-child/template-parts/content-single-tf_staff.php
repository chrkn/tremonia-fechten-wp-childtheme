<?php
/**
 * Template part for displaying posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zakra
 */

$content_orders = get_theme_mod(
	'zakra_single_post_content_structure', array(
		'featured_image',
		'title',
		'meta',
		'content',
	)
);

$meta_orders = get_theme_mod(
	'zakra_single_blog_post_meta_structure', array(
		'comments',
		'categories',
		'author',
		'date',
		'tags',
	)
);

$meta_style = get_theme_mod( 'zakra_blog_archive_meta_style', 'tg-meta-style-one' );

$acf_data = get_fields();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $meta_style ); ?>>
	<?php do_action( 'zakra_before_single_post' ); ?>

			<div class="entry-content tf_staff-single">
				<?php
				the_content(
					sprintf(
						wp_kses(
						    /* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'zakra' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					)
				);

                // render profile card unless deactivated
				if (!$acf_data['steckbrief']) {
                    $attr = [
                        'src'    => $acf_data['bild']['sizes']['tf_staff-flip-card-large-5x3'],
                        'srcset' => $acf_data['bild']['sizes']['tf_staff-flip-card-small-5x3'] . ', ' . $acf_data['bild']['sizes']['tf_staff-flip-card-large-5x3'] . ' 2x'
                    ];
                    ?>
                    <div class="tf_staff-profile-shortinfo">
                        <div class="tf_staff-profile-card-container">
                            <div class="tf_staff-profile-card <?php echo !empty($acf_data['zitat']['text']) && $acf_data['zitat_anzeige'] ? 'tf_staff-profile-card-small' : '' ?>">
                                <?php echo wp_get_attachment_image($acf_data['bild']['ID'], [500, 300], false, $attr) ?>
                                <div class="tf_staff-profile-card-text">
                                    <h3><?php echo $acf_data['vorname'] . ' ' . $acf_data['weitere_vornamen'] . ' ' .  $acf_data['nachname'] ?></h3>
                                    <p class="subline"><?php echo $acf_data['aufgaben']; echo $acf_data['beruf'] ? (' / ' . $acf_data['beruf']) : ''  ?></p>
                                    <ul>
                                        <?php if (!empty($acf_data['alter'])): ?>
                                            <li>geb. <?php echo $acf_data[$acf_data['alter']] ?></li>
                                        <?php endif; ?>
                                        <?php if (!empty($acf_data['hfseit'])): ?>
                                            <li><abbr title="Historisches Fechten">HF</abbr> seit <?php echo $acf_data['hfseit'] ?></li>
                                        <?php endif; ?>
                                        <?php if (!empty($acf_data['favorite1']) || !empty($acf_data['favorite2'])): ?>
                                            <li>
                                                Trainiert besonders gern:<br>
                                                <?php
                                                $training = '';
                                                if (!empty($acf_data['favorite1']) && !empty($acf_data['favorite2']))
                                                    $training = trim($acf_data['favorite1']) . ', ' . trim($acf_data['favorite2']);
                                                elseif (!empty($acf_data['favorite1']))
                                                    $training = trim($acf_data['favorite1']);
                                                elseif ($training = trim($acf_data['favorite2']))
                                                    $training = trim($acf_data['favorite2']);

                                                echo $training;
                                                ?>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                    <?php if (!empty($acf_data['email'])): ?>
                                        <form method="post" action="/kontakt/" class="tf_staff-contact-form">
                                            <input type="hidden" id="your-contact" name="your-contact" value="<?php echo $acf_data['email'] ?>">
                                            <input type="submit" value="Kontakt aufnehmen" class="tf_staff-contact-submit">
                                        </form>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <?php
                        if (!empty($acf_data['zitat']['text']) && $acf_data['zitat_anzeige']):
                            ?><div class="tf_staff-quote-container"><?php
                            echo do_shortcode('[zitat align="flex"]', true);
                            ?></div><?php
                        endif;
                        ?>
                    </div>
                    <?php
                }

				if (!empty($acf_data['kurzbeschreibung'])) {
                    echo $acf_data['kurzbeschreibung'];
                }

                // get image gallery if active
                if ($acf_data['bildergalerie']['bildergalerie_anzeigen']):

                    // get images as needed and todo: randomize structure
                    switch($acf_data['bildergalerie']['anzahl_bilder']) {
                        case 12:
                            $gallery_images['1x1'] = [
                                $acf_data['bildergalerie']['foto_9']['ID'],
                                $acf_data['bildergalerie']['foto_10']['ID'],
                                $acf_data['bildergalerie']['foto_11']['ID'],
                                $acf_data['bildergalerie']['foto_12']['ID']
                            ];
                        case 8:
                            $gallery_images['4x3'] = [
                                $acf_data['bildergalerie']['foto_5']['ID'],
                                $acf_data['bildergalerie']['foto_6']['ID']
                            ];
                            $gallery_images['3x4'] = [
                                $acf_data['bildergalerie']['foto_7']['ID'],
                                $acf_data['bildergalerie']['foto_8']['ID']
                            ];
                        case 4:
                            $gallery_images['3x4'][] = $acf_data['bildergalerie']['foto_3']['ID'];
                            $gallery_images['3x4'][] = $acf_data['bildergalerie']['foto_4']['ID'];
                        case 2:
                            $gallery_images['4x3'][] = $acf_data['bildergalerie']['foto_1']['ID'];
                            $gallery_images['4x3'][] = $acf_data['bildergalerie']['foto_2']['ID'];
                        default:
                            foreach ($gallery_images as $key => $gallery_image) {
                                shuffle($gallery_image);
                                $gallery_images[$key] = $gallery_image;
                            }
                    }
                    $gallery_image_order = [];
                    if ($acf_data['bildergalerie']['anzahl_bilder'] == 2) {
                        $gallery_image_order = [
                            ['4x3'],
                            ['4x3']
                        ];
                    } elseif ($acf_data['bildergalerie']['anzahl_bilder'] == 4) {
                        $gallery_image_order = [
                            ['4x3','3x4'],
                            ['3x4','4x3']
                        ];
                        shuffle($gallery_image_order);
                    } elseif ($acf_data['bildergalerie']['anzahl_bilder'] == 8) {
                        $gallery_image_order = [
                            ['4x3','3x4'],
                            ['3x4','4x3'],
                            ['4x3','3x4'],
                            ['3x4','4x3']
                        ];
                        if (rand(1,10) > 5) {
                            $gallery_image_order[] = ['4x3','3x4'];
                            unset ($gallery_image_order[0]);
                            $gallery_image_order = array_values($gallery_image_order);
                        }
                    } elseif ($acf_data['bildergalerie']['anzahl_bilder'] == 12) {
                        $gallery_image_order_variants = [
                            [
                                ['4x3', '3x4', '1x1'],
                                ['3x4', '1x1', '4x3'],
                                ['1x1', '4x3', '3x4'],
                                ['3x4', '4x3', '1x1']
                            ],
                            [
                                ['1x1', '3x4', '4x3'],
                                ['4x3', '1x1', '3x4'],
                                ['3x4', '4x3', '1x1'],
                                ['1x1', '3x4', '4x3']
                            ],
                            [
                                ['4x3', '1x1', '3x4'],
                                ['3x4', '4x3', '1x1'],
                                ['1x1', '3x4', '4x3'],
                                ['4x3', '1x1', '3x4']
                            ],
                            [
                                ['1x1', '3x4', '4x3'],
                                ['3x4', '4x3', '1x1'],
                                ['4x3', '1x1', '3x4'],
                                ['3x4', '4x3', '1x1']
                            ],
                            [
                                ['1x1', '3x4', '4x3'],
                                ['3x4', '4x3', '1x1'],
                                ['4x3', '1x1', '3x4'],
                                ['1x1', '3x4', '4x3']
                            ]
                        ];

                        $gallery_image_order = $gallery_image_order_variants[rand(0,4)];
                    }

                    // build the html
                    $number_columns = ($acf_data['bildergalerie']['anzahl_bilder'] / 2 > 2) ? 4 : 2;
                    $number_img_per_column = $acf_data['bildergalerie']['anzahl_bilder'] / $number_columns;
                    $images_used = [
                        '4x3' => 0,
                        '3x4' => 0,
                        '1x1' => 0
                    ];
                    ?>
                    <div class="tf_staff-gallery">
                    <?php
                    foreach ($gallery_image_order as $column):
                    ?>
                        <div class="tf_staff-gallery-column-<?php echo $number_columns ?>">
                        <?php
                        foreach ($column as $img_aspect_ratio):
                            ?>
                            <div class="tf_staff-gallery-animation">
                            <picture class="tf_staff-gallery-thumb">
                            <?php
                            $gallery_image_crop = [
                                '1x1-medium-2x' => 'tf_staff-gallery-medium-2x-1x1',
                                '4x3-medium-2x' => 'tf_staff-gallery-medium-2x-4x3',
                                '3x4-medium-2x' => 'tf_staff-gallery-medium-2x-3x4',
                                '1x1-medium-1x' => 'tf_staff-gallery-medium-1x1',
                                '4x3-medium-1x' => 'tf_staff-gallery-medium-4x3',
                                '3x4-medium-1x' => 'tf_staff-gallery-medium-3x4',
                                '1x1-small-2x' => 'tf_staff-gallery-small-2x-1x1',
                                '4x3-small-2x' => 'tf_staff-gallery-small-2x-4x3',
                                '3x4-small-2x' => 'tf_staff-gallery-small-2x-3x4',
                                '1x1-small-1x' => 'tf_staff-gallery-small-1x1',
                                '4x3-small-1x' => 'tf_staff-gallery-small-4x3',
                                '3x4-small-1x' => 'tf_staff-gallery-small-3x4'
                            ];
                            $size_variation = $acf_data['bildergalerie']['anzahl_bilder'] < 5 ? '-medium': '-small';
                            // fallback image
                            $attr = [
                                'class'  => 'tf_staff-gallery-image',
                                'src'    => wp_get_attachment_image_src($gallery_images[$img_aspect_ratio][$images_used[$img_aspect_ratio]], $gallery_image_crop[$img_aspect_ratio . $size_variation . '-2x'])[0],
                                'srcset' => wp_get_attachment_image_src($gallery_images[$img_aspect_ratio][$images_used[$img_aspect_ratio]], $gallery_image_crop[$img_aspect_ratio . $size_variation . '-1x'])[0] . ' 1x, ' . wp_get_attachment_image_src($gallery_images[$img_aspect_ratio][$images_used[$img_aspect_ratio]], $gallery_image_crop[$img_aspect_ratio . $size_variation . '-2x'])[0] . ' 2x'
                            ];
                            echo wp_get_attachment_image(
                                    $gallery_images[$img_aspect_ratio][$images_used[$img_aspect_ratio]],
                                    $gallery_image_crop[$img_aspect_ratio . $size_variation . '-2x'], false, $attr);
                            $images_used[$img_aspect_ratio]++;
                            ?>
                            </picture>
                            </div>
                        <?php
                        endforeach;
                        ?>
                        </div>
                    <?php
                    endforeach;
                    ?>
                    </div>
                <?php
                endif;
                ?>
    <!-- .entry-content -->

	<?php do_action( 'zakra_after_single_post' ); ?>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
