<?php
/**
 * The template for displaying all single posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zakra
 */

handle_html_language_att();
get_header();
?>

	<div id="primary" class="content-area tf_fechtschule-single-content">
		<?php
        get_template_part( 'template-parts/content', 'language_switch' );
        echo apply_filters( 'zakra_after_primary_start_filter', false ); // WPCS: XSS OK. ?>


		<?php
		while ( have_posts() ) :
			the_post();
            $type_of_content = get_field('art_des_beitrags');
            $page_title = get_the_title();
            $registration_open = is_registration_open();

            if (!empty($type_of_content) && $type_of_content == 'static') {
                // static means we are on a general information page, not an actual fechtschule single
                switch (true) {
                    case $page_title == 'Anmeldung' && $registration_open:
                        echo do_shortcode('[contact-form-7 id="2125" title="Anmeldung Fechtschule"]');
                        break;
                    case $page_title == 'Registration' && $registration_open:
                        echo do_shortcode('[contact-form-7 id="2130" title="Registration Fechtschule"]');
                        break;
                    default:
                        echo get_field('seiteninhalt');
                        break;
                }
            }
            else {
                get_template_part( 'template-parts/content-single', get_post_type() );
            }

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		<?php echo apply_filters( 'zakra_after_primary_end_filter', false ); // // WPCS: XSS OK. ?>
	</div><!-- #primary -->

<?php

get_template_part( 'template-parts/content', 'tf_fechtschule-aside' );

get_footer();
