<?php
/**
 * The template for displaying all single posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zakra
 */

$acf_data['profile'] = get_field('profile');
$acf_data['email'] = get_field('email');

if (!$acf_data['profile']) {
    header("Location: /verein/leitung/",true,301);
    exit();
}

get_header();
?>

    <div id="primary" class="content-area">
        <?php echo apply_filters( 'zakra_after_primary_start_filter', false ); // WPCS: XSS OK. ?>

        <?php
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content-single', get_post_type() );

        endwhile; // End of the loop.
        ?>

        <?php echo apply_filters( 'zakra_after_primary_end_filter', false ); // // WPCS: XSS OK. ?>
        <nav class="navigation post-navigation" role="navigation" aria-label="Zurück zur Übersicht">
            <h2 class="screen-reader-text">Zurück zur Übersicht</h2>
            <div class="nav-links"><div class="nav-previous"><a href="/verein/leitung/" rel="prev">Zurück zur Übersicht</a></div></div>
        </nav>
    </div>

    <aside id="secondary" class="tg-site-sidebar widget-area tf_staff-aside">
        <section id="tf_staff-aside" class="widget">
            <h2 class="widget-title">Team Tremonia</h2>
            <ul>
                <?php
                $args = [
                    'post_type'=> 'tf_staff',
                    'order'    => 'DESC'
                ];

                $the_query = new WP_Query( $args );
                if($the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) :
                        $the_query->the_post();
                        $acf_data_aside = get_fields();
                        $url = $acf_data_aside['profile'] ? get_the_permalink() : ('/verein/leitung/#tf_staff-' . get_the_ID());
                        ?>
                        <li class="tf_staff-aside-item">
                            <div <?php echo $acf_data['email'] == $acf_data_aside['email'] ? 'class="tf_staff-aside-current"' : '';  ?>>
                                <a href="<?php echo $url ?>"><?php the_title() ?></a>
                                <br>
                                <span><?php echo $acf_data_aside['aufgaben'] ?></span>
                            </div>
                        </li>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
            </ul>
        </section>
    </aside>
<?php
get_footer();
