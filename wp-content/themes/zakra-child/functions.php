<?php
/*
 * Child theme functions file
 *
 */

// get custom post types
require_once 'cpt/tf-staff.php';
require_once 'cpt/tf-fechtschule.php';
require_once 'cpt/tf-fechtschule-instructor.php';
require_once 'cpt/tf-fechtschule-sponsor.php';
require_once 'cpt/tf-fechtschule-location.php';
require_once 'cpt/tf-fechtschule-course.php';

// get overwrites for default theme functions
include_once 'inc/custom-function-overwrites.php';

// get custom acf fields
require_once 'inc/custom-acf-our-instructors.php';

// get shortcodes
include_once 'inc/custom-shortcodes.php';

// get custom image sizes
include_once 'inc/custom-image-sizes.php';

// get custom functions
require_once 'inc/custom-functions.php';

/**
 * Making sure, theme and child css are being merged
 */
function zakra_child_enqueue_styles() {

	$parent_style = 'zakra-style';

	//Enqueue parent and child theme style.css
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'zakra_child_style',
	    get_stylesheet_directory_uri() . '/style.css',
	    array( $parent_style ),
	    wp_get_theme()->get('Version')
	);
}
add_action( 'wp_enqueue_scripts', 'zakra_child_enqueue_styles' );

/**
 * Remove author from oembed data. Especially for pages that was really, really stupid
 *
 * @param $data
 * @return mixed
 */
function disable_embeds_filter_oembed_response_data_($data ) {
    unset($data['author_url']);
    unset($data['author_name']);
    return $data;
}
add_filter( 'oembed_response_data', 'disable_embeds_filter_oembed_response_data_' );

// give pages an excerpt field (kind of obsolete with yoast now; todo: check if still needed)
add_post_type_support( 'page', 'excerpt' );


/**
 * Uncomment the action to enable maintenance mode
 */
function kb_wartungsmodus() {
    if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {
        wp_die('
    <h1>Wir aktualisieren unsere Website</h1>
    <p>Im Moment arbeiten wir an dieser Website. Bitte schauen Sie in Kürze noch einmal vorbei.</p> ', 'Website im Wartungsmodus');
    }}
//add_action('get_header', 'kb_wartungsmodus');


/**
 * Build the backend menu to create, modify, delete Fechtschul-items
 */
function fechtschule_menu() {
    add_menu_page    ('Fechtschulen', 'Fechtschulen', 'edit_posts',
                      'edit.php?post_type=tf_fechtschule', '', 'dashicons-calendar-alt', 22 );
    add_submenu_page ('edit.php?post_type=tf_fechtschule', 'Dozenten', 'Dozenten', 'edit_posts',
                      'edit.php?post_type=tf_fs_instructor', '', 10 );
    add_submenu_page ('edit.php?post_type=tf_fechtschule', 'Kurse und Programmpunkte', '- Kurse und Programmpunkte', 'edit_posts',
                      'edit.php?post_type=tf_fs_course', '', 20 );
    add_submenu_page ('edit.php?post_type=tf_fechtschule', 'Arten', '-- Arten', 'edit_posts',
                      'edit-tags.php?taxonomy=tf_fs_course_type&post_type=tf_fs_courses', '', 22 );
    add_submenu_page ('edit.php?post_type=tf_fechtschule', 'Zielgruppen', '-- Zielgruppen', 'edit_posts',
                      'edit-tags.php?taxonomy=tf_fs_course_target&post_type=tf_fs_courses', '', 24 );
    add_submenu_page ('edit.php?post_type=tf_fechtschule', 'Waffengattungen', '-- Waffengattungen', 'edit_posts',
                      'edit-tags.php?taxonomy=tf_fs_course_weapon&post_type=tf_fs_courses', '', 26 );
    add_submenu_page ('edit.php?post_type=tf_fechtschule', 'Sponsoren', 'Sponsoren', 'edit_posts',
                      'edit.php?post_type=tf_fs_sponsor', '', 30 );
    add_submenu_page ('edit.php?post_type=tf_fechtschule', 'Orte', 'Orte', 'edit_posts',
                      'edit.php?post_type=tf_fs_location', '', 40 );
}
add_action( 'admin_menu', 'fechtschule_menu' );

/**
 * Making sure that when editing taxonomies belonging to the fechtschule the menu stays open
 */
function tf_fechtschule_parent_file($parent_file) {
    global $current_screen, $self;
    if (in_array($current_screen->base, ['edit-tags', 'post']) && (in_array($current_screen->taxonomy, ['tf_fs_course_target', 'tf_fs_course_type', 'tf_fs_course_weapon']) || in_array($current_screen->post_type, ['tf_fs_instructor', 'tf_fs_course', 'tf_fs_sponsor', 'tf_fs_location']))) {
        $parent_file = 'edit.php?post_type=tf_fechtschule';
    }
    return $parent_file;
}
add_filter( 'parent_file', 'tf_fechtschule_parent_file' );


// to change the open graph image in yoast

//add_filter( 'wpseo_opengraph_image', 'change_opengraph_image_url' );
//
//function change_opengraph_image_url( $url ) {
//    return str_replace('current_domain.com', 'new_domain.com', $url);
//}

add_filter('jpeg_quality', function($arg){return 90;});

// remove google fonts
add_action( 'wp_enqueue_scripts', function() {
    wp_dequeue_style( 'zakra_googlefonts' );
}, 11 );

// add register to the accepted query variabels for the registration
function add_query_vars_filter( $vars ){
    $vars[] = "register";
    return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );