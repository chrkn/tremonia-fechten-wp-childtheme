<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zakra
 */

get_header();
?>

    <div id="primary" class="entry-content">
        <?php echo apply_filters( 'zakra_after_primary_start_filter', false ); // WPCS: XSS OK. ?>
        <?php
        $args = array( 'category' => 50, 'post_type' =>  'post', 'numberposts' => -1 );
        $postslist = get_posts( $args );
        foreach ($postslist as $post) :  setup_postdata($post);
            ?>
            <section class="article-list">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php the_excerpt(); ?>
                <div class="align-right">
                    <a href="<?php the_permalink(); ?>">Mehr lesen</a>
                </div>
            </section>
        <?php endforeach; ?>

        <?php echo apply_filters( 'zakra_after_primary_end_filter', false ); // // WPCS: XSS OK. ?>
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
