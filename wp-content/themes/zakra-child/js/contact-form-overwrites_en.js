let regular = document.querySelector("span.first > input[name^='modi'] + span");
regular.innerHTML = 'Regular price';

let discount = document.querySelector('span.modi > span > span:nth-child(2) > span');
discount.innerHTML = 'Price reduction (- 15 €)';

let sponsor = document.querySelector("span.last > input[name^='modi'] + span");
sponsor.innerHTML = 'Sponsor attendee (+ 15 €)';

let fullprice = document.querySelector('input[name="gesamtpreis"]');
fullprice.setAttribute('prefix', ' €')

let sleep_castle = document.querySelector('span.first > input[name="schlafen"] + span');
sleep_castle.innerHTML = 'Tent (80 €)';

let sleep_external = document.querySelector('span.last > input[name="schlafen"] + span');
sleep_external.innerHTML = 'Own accommodations (80 €)';
