let regular = document.querySelector("span.first > input[name^='modi'] + span");
regular.innerHTML = 'Regulärer Preis';

let discount = document.querySelector('span.modi > span > span:nth-child(2) > span');
discount.innerHTML = 'Ermäßigter Preis (- 15 €)';

let sponsor = document.querySelector("span.last > input[name^='modi'] + span");
sponsor.innerHTML = 'Einen Besucher sponsern (+ 15 €)';

let fullprice = document.querySelector('input[name="gesamtpreis"]');
fullprice.setAttribute('prefix', ' €')

let sleep_castle = document.querySelector('span.first > input[name="schlafen"] + span');
sleep_castle.innerHTML = 'Zelt (80 €)';

let sleep_external = document.querySelector('span.last > input[name="schlafen"] + span');
sleep_external.innerHTML = 'Außerhalb (80 €)';

let club_discount = document.querySelector('input[name^="tremonia"] + span');
club_discount.innerHTML = 'Ja, bitte!'
