<?php

/**
 * Register a custom post type to represent a Fechtschulevent.
 */
function tf_register_cpt_fechtschule() {
    register_post_type('tf_fechtschule',
        [
            'labels'                => [
                'name'          => __('Fechtschule', 'textdomain'),
                'singular_name' => __('Fechtschule', 'textdomain'),
            ],
            'description'           => 'Repräsentiert die jährlich stattfindenden Fechtschulen von Tremonia Fechten.',
            'public'                => true,
            'has_archive'           => true,
            'supports'              => [
                'title', 'revisions'
            ],
            'publicly_queryable'    => true,
            'show_in_menu' => false,
            'rewrite' => [
                'slug' => 'fechtschule'
            ]
        ]
    );
}
add_action('init', 'tf_register_cpt_fechtschule');
