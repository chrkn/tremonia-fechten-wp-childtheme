<?php

/**
 * Register a custom post type to represent a location for the Fechtschule.
 */
function tf_register_cpt_location() {
    register_post_type('tf_fs_location',
        [
            'labels'                => [
                'name'          => __('Orte', 'textdomain'),
                'singular_name' => __('Ort', 'textdomain'),
            ],
            'description'           => 'Repräsentiert einen Ort an dem die Tremonia Fechtschule stattfindet.',
            'public'                => true,
            'has_archive'           => false,
            'supports'              => [
                'title', 'revisions'
            ],
            'publicly_queryable'    => true,
            'rewrite' => [
                'slug' => 'fechtschule/ort'
            ],
            'show_in_menu'          => false,
        ]
    );
}
add_action('init', 'tf_register_cpt_location');