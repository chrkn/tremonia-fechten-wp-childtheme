<?php

/**
 * Register a custom post type to represent a sponsor for the Fechtschule.
 */
function tf_register_cpt_sponsor() {
    register_post_type('tf_fs_sponsor',
        [
            'labels'                => [
                'name'          => __('Sponsoren', 'textdomain'),
                'singular_name' => __('Sponsor', 'textdomain'),
            ],
            'description'           => 'Repräsentiert einen Sponsor der Tremonia Fechtschule.',
            'public'                => true,
            'has_archive'           => false,
            'supports'              => [
                'title', 'revisions'
            ],
            'publicly_queryable'    => true,
            'rewrite' => [
                'slug' => 'fechtschule/sponsor'
            ],
            'show_in_menu'          => false,
        ]
    );
}
add_action('init', 'tf_register_cpt_sponsor');