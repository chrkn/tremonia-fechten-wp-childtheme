<?php

function tf_register_cpt_staff() {
    register_post_type('tf_staff',
        [
            'labels'                => [
                'name'          => __('Leitung', 'textdomain'),
                'singular_name' => __('Leitung', 'textdomain'),
            ],
            'description'           => 'Trainer und sonstige Offizielle von Tremonia Fechten, 
                                    die auf der Webseite angegeben werden sollen',
            'public'                => true,
            'has_archive'           => true,
            'supports'              => [
                'title', 'revisions'
            ],
            'rewrite'               => [
                'slug'  => 'verein/leitung',
            ],
            'publicly_queryable'    => true,
            'menu_position'         => 21,
            'menu_icon'             => 'dashicons-id-alt'
        ]
    );
}
add_action('init', 'tf_register_cpt_staff');
