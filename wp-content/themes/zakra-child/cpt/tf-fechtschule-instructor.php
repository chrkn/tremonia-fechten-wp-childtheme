<?php

/**
 * Register a custom post type to represent an instructor for the Fechtschule.
 */
function tf_register_cpt_instructor() {
    register_post_type('tf_fs_instructor',
        [
            'labels'                => [
                'name'          => __('Dozenten', 'textdomain'),
                'singular_name' => __('Dozent', 'textdomain'),
            ],
            'description'           => 'Repräsentiert einen Dozenten der Tremonia Fechtschule.',
            'public'                => true,
            'has_archive'           => false,
            'supports'              => [
                'title', 'revisions'
            ],
            'publicly_queryable'    => true,
            'rewrite' => [
                'slug' => 'fechtschule/dozent'
            ],
            'show_in_menu'          => false,
        ]
    );
}
add_action('init', 'tf_register_cpt_instructor');