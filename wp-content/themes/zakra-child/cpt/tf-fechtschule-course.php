<?php

/**
 * Register a custom post type to represent a course at the Fechtschule.
 */
function tf_register_cpt_course() {
    register_post_type('tf_fs_course',
        [
            'labels'                => [
                'name'          => __('Seminare', 'textdomain'),
                'singular_name' => __('Seminar', 'textdomain'),
            ],
            'description'           => 'Repräsentiert einen Kurs auf der Tremonia Fechtschule.',
            'public'                => true,
            'has_archive'           => false,
            'supports'              => [
                'title', 'revisions'
            ],
            'publicly_queryable'    => true,
            'rewrite' => [
                'slug' => 'fechtschule/seminar'
            ],
            'show_in_menu'          => false,
            'taxonomies'            => [
                'tf_fs_course_type',
                'tf_fs_course_target',
            ]
        ]
    );
}
add_action('init', 'tf_register_cpt_course');


function tf_register_tax_type() {
    register_taxonomy('tf_fs_course_type', 'tf_fs_course',
        [
            'labels'                => [
                'name'          => __('Seminararten', 'taxonomy general name'),
                'singular_name' => __('Seminarart', 'taxonomy singular name'),
                'all_items' => 'Alle Seminararten anzeigen',
                'new_item_name' => 'Neuen Namen festlegen',
                'edit_item' => 'Seminarart bearbeiten',
                'view_item' => 'Seminarart ansehen',
                'update_item' => 'Seminarart aktualisieren',
                'add_new_item' => 'Neue Seminarart anlegen',
                'search_items' => 'Seminararten durchsuchen',
                'popular_items' => 'Häufige Seminararten',
                'separate_items_with_commas' => 'Mehrere Seminararten mit Komma trennen',
                'add_or_remove_items' => 'Seminarart erstellen oder löschen',
                'choose_from_most_used' => 'Aus häufigen Seminararten wählen',
                'not_found' => 'Keine passende Seminarart gefunden',
                'back_to_items' => 'Zurück zur Übersicht',
            ],
            'description' => 'Art des Seminars/Kurses. Zum Beispiel \'Workshop\' oder \'Vortrag\'.',
            'public' => true,
            'rewrite' => [
                'slug' => 'fechtschule/seminare/art'
            ],
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
        ]
    );
}
add_action('init', 'tf_register_tax_type');

function tf_register_tax_target_group() {
    register_taxonomy('tf_fs_course_target', 'tf_fs_course',
        [
            'labels'                => [
                'name'          => __('Zielgruppen', 'taxonomy general name'),
                'singular_name' => __('Zielgruppe', 'taxonomy singular name'),
                'all_items' => 'Alle Zielgruppen anzeigen',
                'new_item_name' => 'Neuen Namen festlegen',
                'edit_item' => 'Zielgruppe bearbeiten',
                'view_item' => 'Zielgruppe ansehen',
                'update_item' => 'Zielgruppe aktualisieren',
                'add_new_item' => 'Neue Zielgruppe anlegen',
                'search_items' => 'Zielgruppen durchsuchen',
                'popular_items' => 'Häufige Zielgruppen',
                'separate_items_with_commas' => 'Mehrere Zielgruppen mit Komma trennen',
                'add_or_remove_items' => 'Zielgruppe erstellen oder löschen',
                'choose_from_most_used' => 'Aus häufigen Zielgruppen wählen',
                'not_found' => 'Keine passende Zielgruppe gefunden',
                'back_to_items' => 'Zurück zur Übersicht',
            ],
            'description' => 'Zielgruppe des Kurses. Zum Beispiel \'Anfänger\' oder \'Alle\'.',
            'public' => true,
            'rewrite' => [
                'slug' => 'fechtschule/seminare/zielgruppe'
            ],
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
        ]
    );
}
add_action('init', 'tf_register_tax_target_group');

function tf_register_tax_weapon() {
    register_taxonomy('tf_fs_course_weapon', 'tf_fs_course',
        [
            'labels'                => [
                'name'          => __('Waffengattungen', 'taxonomy general name'),
                'singular_name' => __('Waffengattung', 'taxonomy singular name'),
                'all_items' => 'Alle Waffengattungen anzeigen',
                'new_item_name' => 'Neuen Namen festlegen',
                'edit_item' => 'Waffengattung bearbeiten',
                'view_item' => 'Waffengattung ansehen',
                'update_item' => 'Waffengattung aktualisieren',
                'add_new_item' => 'Neue Waffengattung anlegen',
                'search_items' => 'Waffengattungen durchsuchen',
                'popular_items' => 'Häufige Waffengattungen',
                'separate_items_with_commas' => 'Mehrere Waffengattungen mit Komma trennen',
                'add_or_remove_items' => 'Waffengattung erstellen oder löschen',
                'choose_from_most_used' => 'Aus häufigen Waffengattungen wählen',
                'not_found' => 'Keine passende Waffengattung gefunden',
                'back_to_items' => 'Zurück zur Übersicht',
            ],
            'description' => 'Waffengattung eines Kurses, z.B. \'Langes Schwert\' oder \'Scheibendolch\'.',
            'public' => true,
            'rewrite' => [
                'slug' => 'fechtschule/seminare/waffengattungen'
            ],
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
        ]
    );
}
add_action('init', 'tf_register_tax_weapon');